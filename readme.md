cardi-c
==========

![https://bit.ly/3J20nnD](https://bit.ly/3J20nnD)

A Datadog metrics **cardi**nality **c**alculator - _not just another female rapper w/ a fast flow :)_

## description

- Run `Datadog Synthetics Tests` vs. 3rd-party `crystal-lang` `API` running on `Heroku`
- Pass Datadog credentials in the standard way via `HTTP headers`
- **Delegate all the heavy lifting** _(`Datadog API` calls)_ to said 3rd-party `API`
- `Crystal API` responds w/ concatenated `Datadog API` responses _(acts as a **smart proxy**)_
- **Bonus** : Crystal API **also** _**ships responses as logs**_ to Datadog to allow in-app creation of `custom metrics from logs`
- **Benefit** : This makes it possible to _**track & alert on Datadog metrics spikes & cardinality spikes in**_ **realtime**

### usage

#### runs from heroku

https://cardi-c.herokuapp.com/

Expects Datadog credentials to be passed as headers - will respond w/ `400` if ecxpected headers are missing

#### heroku query

    curl \
	    -sw'\n' \
	    -H "DD-API-KEY: $DD_API_KEY" \
	    -H"DD-APPLICATION-KEY: $DD_APPLICATION_KEY" \
	    -H"DD-SITE: $DD_SITE" \
	    https://cardi-c.herokuapp.com/

#### local run

    # crystal cardi-c.cr
    [production] Kemal is ready to lead at http://0.0.0.0:8080
    2022-02-03 18:58:49 UTC 200 GET / 3031.61ms

#### local build & run compiled version

    # shards install
    # shards build
    # bin/cardinality-calculator
    [production] Kemal is ready to lead at http://0.0.0.0:8080
	  2022-02-03 18:58:49 UTC 200 GET / 3031.61ms

#### local query

    curl \
	    -sw'\n' \
	    -H "DD-API-KEY: $DD_API_KEY" \
	    -H"DD-APPLICATION-KEY: $DD_APPLICATION_KEY" \
	    -H"DD-SITE: $DD_SITE" \
	    127.0.0.1:8080

### endpoints

Datadog API Endpoints used :

- active metrics
- top average custom metrics
- metrics metadata
- metrics volumes
- metrics tags
