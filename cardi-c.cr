require "kemal"
require "json"
before_all { |http| http.response.content_type = "application/json"  }
get "/"    { |http| process http }
error 404  { |http|
    http.response.content_type = "application/json"
    r = http.request
    routes = ["GET /"]
    { error: true, method: "#{r.method}", path: "#{r.path}", supported_routes: routes, message: "no such route #{r.method} #{r.path}" }.to_json
}
DEBUG = ENV["DEBUG"] ||= "false"
begin
    env  = ENV["ENV"]  ||= "production"
    bind = ENV["BIND"] ||= "0.0.0.0"
    port = ENV["PORT"] ||= "8080"
    port = port.to_i
    Kemal.run { |config|
      server = config.server.not_nil!
      server.bind_tcp bind, port
      config.env = env
    }
rescue error
    STDERR.puts error.message
    exit 1
end
def process(http : HTTP::Server::Context)
    params = http.params.query.to_h
    begin
        r = http.request
        api_key = r.headers["DD-API-KEY"]
        app_key = r.headers["DD-APPLICATION-KEY"]
        tld =  r.headers["DD-SITE"]
        headers = HTTP::Headers{"User-Agent" => "metrics-watchdog","DD-API-KEY" => api_key,"DD-APPLICATION-KEY" => app_key,"Content-Type" => "application/json"}
        ## VALIDATE API KEY
        version = 1
        endpoint = "validate"
        url = "https://api.datadoghq.#{tld}/api/v#{version}/#{endpoint}"
        request  = HTTP::Client.get(url,headers)
        ## GET ORG DETAILS
        version = 1
        endpoint = "org"
        url = "https://api.datadoghq.#{tld}/api/v#{version}/#{endpoint}"
        request  = HTTP::Client.get(url,headers)
        params.has_key?("filter") && ( filter = params["filter"] ) || ( filter = "UNDEF" )
        buffer = [] of String
        ## GET TOP CUSTOM METRICS FROM USAGE
        version = 1
        endpoint = "usage/top_avg_metrics"
        url = "https://api.datadoghq.#{tld}/api/v#{version}/#{endpoint}?day=#{Time.utc.to_s("%Y-%m-%d")}"
        r  = HTTP::Client.get(url,headers)
        details = {
            "url" => url,
            "status_code" => r.status_code,
            "body" => JSON.parse(r.body),
            "filter" => filter,
            "headers" => {
                "x-ratelimit-remaining" => r.headers["x-ratelimit-remaining"],
                "x-ratelimit-limit" => r.headers["x-ratelimit-limit"],
                "x-ratelimit-period" => r.headers["x-ratelimit-period"],
                "x-ratelimit-reset" => r.headers["x-ratelimit-reset"]
            }
        }.to_json
        buffer.push details
        ## GET ACTIVE METRICS LIST
        version = 1
        endpoint = "metrics"
        from = Time.utc.shift(-86400 ,0 ).to_unix
        url = "https://api.datadoghq.#{tld}/api/v#{version}/#{endpoint}?from=#{from}"
        r  = HTTP::Client.get(url,headers)
        details = {
            "url" => url,
            "status_code" => r.status_code,
            "body" => JSON.parse(r.body),
            "filter" => filter,
            "headers" => {
                "x-ratelimit-remaining" => r.headers["x-ratelimit-remaining"],
                "x-ratelimit-limit" => r.headers["x-ratelimit-limit"],
                "x-ratelimit-period" => r.headers["x-ratelimit-period"],
                "x-ratelimit-reset" => r.headers["x-ratelimit-reset"]
            }
        }.to_json
        buffer.push details
        JSON.parse(r.body)["metrics"].as_a.each { |metric|
            ## REGEX CHECK IF SHOULD PROCESS CURRENT METRIC OR NOT
            if /^#{filter}.*$/i.match(metric.to_s)
                DEBUG == true && STDERR.puts "processing #{metric} - matched #{filter}"
            else
                DEBUG == true && STDERR.puts "skipping #{metric} - did not match #{filter}"
                next
            end
            ## GET METADATA FOR CURRENT METRIC
            version = 1
            endpoint = metric
            url = "https://api.datadoghq.#{tld}/api/v#{version}/metrics/#{endpoint}"
            r  = HTTP::Client.get(url,headers)
            details = {
                "url" => url,
                "status_code" => r.status_code,
                "body" => JSON.parse(r.body),
                "headers" => {
                    "x-ratelimit-remaining" => r.headers["x-ratelimit-remaining"],
                    "x-ratelimit-limit" => r.headers["x-ratelimit-limit"],
                    "x-ratelimit-period" => r.headers["x-ratelimit-period"],
                    "x-ratelimit-reset" => r.headers["x-ratelimit-reset"]
                }
            }.to_json
            buffer.push details
            ## GET VOLUMES FOR CURRENT METRIC
            version = 2
            endpoint = "volumes"
            url = "https://api.datadoghq.#{tld}/api/v#{version}/metrics/#{metric}/#{endpoint}"
            r  = HTTP::Client.get(url,headers)
            details = {
                "url" => url,
                "status_code" => r.status_code,
                "body" => JSON.parse(r.body),
                "parsed_body" => JSON.parse(r.body)["data"]["attributes"]["distinct_volume"] || nil ,
                "headers" => {
                    "x-ratelimit-name" => r.headers["x-ratelimit-name"],
                    "x-ratelimit-remaining" => r.headers["x-ratelimit-remaining"],
                    "x-ratelimit-limit" => r.headers["x-ratelimit-limit"],
                    "x-ratelimit-period" => r.headers["x-ratelimit-period"],
                    "x-ratelimit-reset" => r.headers["x-ratelimit-reset"]
                }
            }.to_json
            buffer.push details
            ## GET TAGS FOR CURRENT METRIC
            version = 2
            endpoint = "all-tags"
            url = "https://api.datadoghq.#{tld}/api/v#{version}/metrics/#{metric}/#{endpoint}"
            r  = HTTP::Client.get(url,headers)
            details = {
                "url" => url,
                "status_code" => r.status_code,
                "body" => JSON.parse(r.body),
                "parsed_body" => JSON.parse(r.body)["data"]["attributes"]["tags"] || nil ,
                "cardinality" => JSON.parse(r.body)["data"]["attributes"]["tags"].size || nil ,
                "headers" => {
                    "x-ratelimit-name" => r.headers["x-ratelimit-name"],
                    "x-ratelimit-remaining" => r.headers["x-ratelimit-remaining"],
                    "x-ratelimit-limit" => r.headers["x-ratelimit-limit"],
                    "x-ratelimit-period" => r.headers["x-ratelimit-period"],
                    "x-ratelimit-reset" => r.headers["x-ratelimit-reset"]
                }
            }.to_json
            buffer.push details
        }
        ## SEND RESPONSES AS LOGS
        url = "https://http-intake.logs.datadoghq.#{tld}/api/v2/logs"
        buffer.each { |item|
            body = item
            logs_config = <<-EOF
            {
                "@timestamp" : "#{Time.utc.to_s("%Y-%m-%dT%H:%M:%S.000Z")}",
                "ddsource" : "metrics-watchdog",
                "ddtags" : "metrics-watchdog,random:#{Random.new.next_int}",
                "hostname" : "metrics-watchdog.herokuapp.com",
                "service" : "metrics-watchdog"
            }
            EOF
            body = JSON.parse(body).as_h.merge!(JSON.parse(logs_config).as_h).to_json
            r  = HTTP::Client.post(url,headers,body)
        }
        response = JSON.parse("[" + buffer.join(",") + "]")
        DEBUG == true && STDERR.puts response.to_pretty_json
        body = response
    rescue error : ArgumentError|JSON::ParseException|KeyError|TypeCastError|JSON::SerializableError
        http.response.status_code = 400
        response = { error: true, message: error.message.to_s.split("\n").first, payload_received: body || nil }
    end
    params.has_key?("pretty") && ( return response.to_pretty_json ) || ( return response.to_json )
end
